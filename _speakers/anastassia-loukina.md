---
name: Anastassia Loukina
talks:
- Building a Read Aloud eBook in Python
---

Anastassia Loukina is a research scientist at ETS Speech and NLP group. She holds a DPhil in phonetics. She has been using Python since 2008 for both research and operational applications related to speech analysis and automated scoring in general.
