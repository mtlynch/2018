---
abstract: "Reviewing code is like driving: everyone thinks they\u2019re good at it.
  The dev who just sent you 25 brusque, vague notes thinks of themselves as a wonderful
  reviewer. Truly great reviewers are great because they consider the human factors
  of reviews. So come learn how to do code reviews like a human."
duration: 25
level: All
room: Madison
slot: 2018-10-06 15:40:00-04:00
speakers:
- Michael Lynch
title: How to Do Code Reviews Like a Human
type: talk
video_url: https://youtu.be/0t4_MfHgb_A
---

Are code reviews a source of tension on your team? Do they lead to
conflict, bikeshedding, or wasted time?

Most of the discussion we hear about code reviews is technical. We focus
myopically on minimizing cost and maximizing bug discoveries. But reviews
are as much a social exercise as a technical one. They’re an opportunity for
teammates to share knowledge and bond through collaboration. That can’t
happen if people are strangling each other over where to place the curly
braces.

In this talk, you’ll learn practical techniques your team can use to improve
communication and minimize conflict during code reviews. I’ll talk about:

* Reducing human effort of reviews through automation
* Increasing your team’s review velocity
* Giving sincere praise
* Framing discussions for constructive debate
* Mitigating stalemates