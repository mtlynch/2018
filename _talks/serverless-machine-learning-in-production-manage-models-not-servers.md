---
abstract: "Putting machine learning models into production is difficult - especially
  for a team that isn\u2019t cross-function and doesn\u2019t have strong ops or software
  engineering support.  We used severless technology to deploy a ML system that let
  data scientists rapidly put models they trained into production"
duration: 25
level: Intermediate
room: Madison
slot: 2018-10-06 14:00:00-04:00
speakers:
- William Cox
title: 'Serverless machine learning in production: manage models, not servers '
type: talk
video_url: https://youtu.be/IXqkDClqyuc
---

Putting machine learning models into production is difficult. It’s especially hard to do for a team that isn’t cross-function and doesn’t have strong ops or software engineering support. At Distil we used severless technology to deploy a Python-based machine learning system that let data scientists rapidly put models they trained into production for detecting automated threats on websites. This talk will discuss how we rapidly built this pipeline, what we learned and what we’d do differently in the future. We make hundreds of predictions per second and stream the results to hundreds of machines all without having to manage any machines or hardware.